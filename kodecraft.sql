/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : kodecraft

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-12-07 20:56:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pemohon
-- ----------------------------
DROP TABLE IF EXISTS `pemohon`;
CREATE TABLE `pemohon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `noktp` varchar(16) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `totalskor` varchar(255) DEFAULT NULL,
  `jawaban` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pemohon
-- ----------------------------
INSERT INTO `pemohon` VALUES ('1', '2020-12-05 21:06:51', '1234567', null, '80', '12;8;6;29;57;5;3;15;74;3;5;87;35;5;7;16;75;tidak ada;tidak ada;tidak ada;tidak ada;26;42;35;96;');
INSERT INTO `pemohon` VALUES ('2', '2020-12-05 21:07:13', '1234567', null, '80', null);

-- ----------------------------
-- Table structure for soal
-- ----------------------------
DROP TABLE IF EXISTS `soal`;
CREATE TABLE `soal` (
  `nomer` int(2) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `jawaban` varchar(9) DEFAULT NULL,
  `a` varchar(9) DEFAULT NULL,
  `b` varchar(9) DEFAULT NULL,
  `c` varchar(9) DEFAULT NULL,
  `d` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of soal
-- ----------------------------
INSERT INTO `soal` VALUES ('1', '01.jpg', '12', '12', '10', '11', 'tidak ada');
INSERT INTO `soal` VALUES ('2', '02.jpg', '8', '9', '6', '8', 'tidak ada');
INSERT INTO `soal` VALUES ('3', '03.jpg', '6', '9', '6', '8', 'tidak ada');
INSERT INTO `soal` VALUES ('4', '04.jpg', '29', '20', '29', '39', 'tidak ada');
INSERT INTO `soal` VALUES ('5', '05.jpg', '57', '67', '47', '57', 'tidak ada');
INSERT INTO `soal` VALUES ('6', '06.jpg', '5', '5', '6', '4', 'tidak ada');
INSERT INTO `soal` VALUES ('7', '07.jpg', '3', '4', '3', '2', 'tidak ada');
INSERT INTO `soal` VALUES ('8', '08.jpg', '15', '16', '17', '15', 'tidak ada');
INSERT INTO `soal` VALUES ('9', '09.jpg', '74', '75', '76', '74', 'tidak ada');
INSERT INTO `soal` VALUES ('10', '10.jpg', '2', '3', '2', '4', 'tidak ada');
INSERT INTO `soal` VALUES ('11', '11.jpg', '6', '5', '4', '6', 'tidak ada');
INSERT INTO `soal` VALUES ('12', '12.jpg', '97', '87', '67', '97', 'tidak ada');
INSERT INTO `soal` VALUES ('13', '13.jpg', '45', '45', '35', '55', 'tidak ada');
INSERT INTO `soal` VALUES ('14', '14.jpg', '5', '4', '5', '6', 'tidak ada');
INSERT INTO `soal` VALUES ('15', '15.jpg', '7', '7', '6', '8', 'tidak ada');
INSERT INTO `soal` VALUES ('16', '16.jpg', '16', '16', '18', '19', 'tidak ada');
INSERT INTO `soal` VALUES ('17', '17.jpg', '73', '73', '74', '75', 'tidak ada');
INSERT INTO `soal` VALUES ('18', '18.jpg', 'tidak ada', '5', '8', '7', 'tidak ada');
INSERT INTO `soal` VALUES ('19', '19.jpg', 'tidak ada', '2', '3', '4', 'tidak ada');
INSERT INTO `soal` VALUES ('20', '20.jpg', 'tidak ada', '46', '45', '44', 'tidak ada');
INSERT INTO `soal` VALUES ('21', '21.jpg', 'tidak ada', '77', '73', '74', 'tidak ada');
INSERT INTO `soal` VALUES ('22', '22.jpg', '26', '2', '6', '26', 'tidak ada');
INSERT INTO `soal` VALUES ('23', '23.jpg', '42', '42', '4', '2', 'tidak ada');
INSERT INTO `soal` VALUES ('24', '24.jpg', '35', '3', '5', '35', 'tidak ada');
INSERT INTO `soal` VALUES ('25', '25.jpg', '96', '9', '6', '96', 'tidak ada');

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database();
	}
	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function petunjuk(){
		//simpan noktp di DB
		$noktp = $this->input->post('noktp');
		$data = array('noktp'=> $noktp);
		$this->db->insert('pemohon', $data);
		
		//taruh noktp di session
		$this->session->set_userdata('noktp', $noktp);
		//tampilkan petunjuk
		$this->load->view('petunjuk');
	}

	public function test(){
		$noktp = $this->session->noktp;
		//cek soal terakhir yang udah dijawab, kemudian tampilkan soal berikutnya
		$nomer_berikutnya = $this->cekSoalTerakhir($noktp);
		$query_soal = $this->db->get_where('soal', array('nomer	' => $nomer_berikutnya), 1);
		if(!empty($query_soal->row())){
			$data['soal'] = $query_soal->row();
			$this->load->view('test', $data);
		}else{
			$data['hasil'] = $this->hasil($noktp);
			$this->load->view('hasil', $data);
		}
	}

	//fungsi untuk cek soal terakhir
	public function cekSoalTerakhir($noktp){
		$query 			= $this->db->get_where('pemohon', array('noktp' => $noktp), 1);
		$jawaban 		= $query->row()->jawaban;
		$soal_terjawab 	= explode(";", $jawaban);
		
		$nomer_berikutnya = count($soal_terjawab);
		$soal_terjawab = array_filter($soal_terjawab);
		// echo "<pre>";
		// print_r($soal_terjawab);
		// echo "</pre>";
		//update skor
		// ambil kunci jawaban
		$this->db->select('jawaban');
		$query_kunci	= $this->db->get('soal');
		$kunci = array_values($query_kunci->result_array());
		$nilai = 0;
		foreach ($soal_terjawab as $key => $value){
			if($value == $kunci[$key]['jawaban']){
				$nilai = $nilai + 4;
			}
		}
		$nilai_baru = array('totalskor' => $nilai);
		$this->db->where('noktp', $noktp);
		$this->db->update('pemohon', $nilai_baru);

		
		return $nomer_berikutnya;
	}

	//fungsi simpan jawaban
	public function jawab(){
		$noktp = $this->session->noktp;
		//ambil deretan jawaban yang udah masuk
		$query 			= $this->db->get_where('pemohon', array('noktp' => $noktp), 1);
		$jawaban_lama	= $query->row()->jawaban;

		//simpan jawaban
		$jawaban_baru = array("jawaban" => $jawaban_lama.$this->input->post('jawaban').";");
		
		$this->db->where('noktp', $noktp);
		$this->db->update('pemohon', $jawaban_baru);
		
		//balik ke test lagi
		$this->test();
	}

	public function hasil($noktp){
		$query 			= $this->db->get_where('pemohon', array('noktp' => $noktp), 1);
		$skor	= $query->row()->totalskor;
		if($skor >= 84){
			$hasil = "normal";
		}elseif($skor >=32){
			$hasil = "buta warna parsial";
		}else{
			$hasil = "buta warna total";
		}
		return $hasil;
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Test Buta Warna Bagi Pemohon SIM</title>
</head>
<body>
	<div>
	<p>Soal ke <?php echo $soal->nomer?> dari 25</p>
    <img src="<?php echo base_url()?>assets/ishihara/<?php echo $soal->gambar?>">
    <?php
        echo form_open('welcome/jawab');
        echo form_hidden('nomer', $soal->nomer);
        echo form_radio('jawaban', $soal->a);
        echo "A.". $soal->a;
        echo form_radio('jawaban', $soal->b);
        echo "B.". $soal->b;
        echo form_radio('jawaban', $soal->c);
        echo "C.". $soal->c;
        echo form_radio('jawaban', $soal->d);
        echo "D.". $soal->d."</br>";
		echo form_submit('submit','Berikutnya');
		echo form_close();
	?>
	</div>
</body>
</html>